const video = document.getElementById('video')
console.log('loaded')
console.log(video)
/* Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models'),
  faceapi.nets.ageGenderNet.loadFromUri('/models')
]).then(startVideo)
*/

async function loadModels () {
  console.log('loadmodels')
  await faceapi.nets.tinyFaceDetector.loadFromUri('/models')
  await faceapi.nets.faceLandmark68Net.loadFromUri('/models')
  await faceapi.nets.faceRecognitionNet.loadFromUri('/models')
  await faceapi.nets.faceExpressionNet.loadFromUri('/models')
  await faceapi.nets.ageGenderNet.loadFromUri('/models')
  startVideo()
}
loadModels()
function startVideo () {
  console.log('startvideo')
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

video.addEventListener('play', () => {
  console.log('video is playing')
  const canvas = faceapi.createCanvasFromMedia(video)
  console.log(1)
  document.body.append(canvas)
  console.log(2)
  const displaySize = { width: video.width, height: video.height }
  console.log(3)
  faceapi.matchDimensions(canvas, displaySize)
  console.log(4)
  setInterval(async () => {
    console.log(5)
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withAgeAndGender()
    console.log(6)
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
    console.log(7)
    console.log('detections')
    console.log(detections)
    if (detections.length > 0) {
      var age = document.getElementById('age')
      var gender = document.getElementById('gender')
      var expression = document.getElementById('expression')

      age.innerText = detections[0].agetoFixed(0)
      gender.innerText = detections[0].gender

      // begin with the first value
      var detected_expression = 'angry'
      var detected_expression_value = detections[0].expressions.angry
      //
      if (detections[0].expressions.disgusted > detected_expression_value) {
        detected_expression_value = detections[0].expressions.disgusted
        detected_expression = 'disgusted'
      }
      //
      if (detections[0].expressions.fearful > detected_expression_value) {
        detected_expression_value = detections[0].expressions.fearful
        detected_expression = 'fearful'
      }
      //
      if (detections[0].expressions.happy > detected_expression_value) {
        detected_expression_value = detections[0].expressions.happy
        detected_expression = 'happy'
      }
      //
      if (detections[0].expressions.neutral > detected_expression_value) {
        detected_expression_value = detections[0].expressions.neutral
        detected_expression = 'neutral'
      }
      //
      if (detections[0].expressions.sad > detected_expression_value) {
        detected_expression_value = detections[0].expressions.sad
        detected_expression = 'sad'
      }
      //
      if (detections[0].expressions.surprised > detected_expression_value) {
        detected_expression_value = detections[0].expressions.surprised
        detected_expression = 'surprised'
      }

      expression.innerText = detected_expression
    }
  }, 100)
})
