import layout from 'layouts/MyLayout'
import beverages from 'pages/beverages'
import alphabet from 'pages/alphabet'
import numbers from 'pages/numbers'
import chemistry from 'pages/chemistry'
import faceScan from 'pages/faceScan'

const routes = [{
  path: '/',
  component: layout,
  children: [{
    path: '',
    component: alphabet
  },
  {
    path: '/alphabet',
    component: alphabet
  },
  {
    path: '/chemistry',
    component: chemistry
  },
  {
    path: '/numbers',
    component: numbers
  },
  {
    path: '/beverages',
    component: beverages
  },
  {
    path: '/faceScan',
    component: faceScan
  }
  ]
}]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () =>
            import('pages/Error404.vue')
  })
}

export default routes
